FROM snakemake/snakemake:v7.32.4

RUN mamba install -c bioconda -c conda-forge numpy=1.26.4
RUN mamba install -c bioconda -c conda-forge tk=8.6.12
RUN pip install opencv-python


WORKDIR /ab

COPY . /ab