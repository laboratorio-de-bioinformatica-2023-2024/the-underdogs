import cv2 as cv
import os
import glob

# Caminho para a pasta das imagens de entrada
input_folder_path = "./Imagens_ovos_Drosphilla"
# Caminho para a pasta de saída onde as imagens processadas serão salvas
output_folder_path = "./Imagens_Output_2"

# Verifica se a pasta de saida existe e cria uma se não existir
if not os.path.exists(output_folder_path):
    os.makedirs(output_folder_path)

# Lista todos os arquivos de imagem na pasta de entrada
image_files = glob.glob(os.path.join(input_folder_path, "*.jpg"))

# Abre um txt para guardar o numero de ovos contados
output_text_file_path = os.path.join(output_folder_path, "egg_counts.txt")
output_text_file = open(output_text_file_path, "w")

# Loop sobre cada arquivo de imagem na pasta
for image_file in image_files:
    # Carregar imagem
    image = cv.imread(image_file)
    
    # Converte a imagem para cinza
    gray_image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

    # Desfoca usando o metodo gaussiano para reduzir o ruido e melhorar a detecção de bordas
    blurred = cv.GaussianBlur(gray_image, (7, 7), 0)

    # Executa a deteção de bordas
    edges = cv.Canny(blurred, 30, 80)

    # Encontra contornos na imagem de borda
    _, contours, _ = cv.findContours(edges.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)

    # Filtra os contornos por tamanho para identificar potenciais ovos
    min_egg_area = 20
    max_egg_area = 500
    egg_contours = [cnt for cnt in contours if min_egg_area < cv.contourArea(cnt) < max_egg_area]

    # Contar o numero de ovos identificados
    egg_count = len(egg_contours)

    # Desenha contornos ao redor dos ovos identificados para visualização
    contoured_image = cv.cvtColor(gray_image, cv.COLOR_GRAY2BGR)
    cv.drawContours(contoured_image, egg_contours, -1, (0, 255, 255), 2)

    # Guarda a imagem processada numa pasta
    output_file_path = os.path.join(output_folder_path, os.path.basename(image_file))
    cv.imwrite(output_file_path, contoured_image)

    # Escreve o numero de ovos contados no arquivo de txt
    output_text_file.write(f"{os.path.basename(image_file)}: {egg_count}\n")

    # Imprime uma mensagem de confirmação
    print(f"Processed image saved: {output_file_path}, Egg count: {egg_count}")

output_text_file.close()
print("All images processed and saved successfully.")
