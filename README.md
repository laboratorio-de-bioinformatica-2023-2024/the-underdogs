# The Underdogs

## 📖 Sobre

Este programa foi desenvolvido para a criação de um contador de ovos da espécie de Drosophila.

Aqui está uma visão geral do pipeline:

Uma interface amigável solicitará um unico input: Imagem a qual seram contados o número de ovos.

O programa permite que o usuário selecionar manualmente uma área de interesse em uma imagem. Essa área contém ovos que serão analisados.

A imagem da área de interesse é convertida para tons de cinza, e em seguida, é aplicada uma técnica de limiarização de Otsu para criar uma imagem binária, onde os ovos se destacam em relação ao fundo.

Com base na imagem binária, o programa encontra os contornos dos objetos (ovos) presentes na imagem e conta quantos ovos existem.

Por fim o utilizador poderá ainda solicitar a precisão da contagem da imagem que solicitou onde o programa irá repetir o processo 10 000 vezes de modo a criar uma percentagem sobre a precisão que o modelo obteve em relação aquela imagem.

Para o 2º programa o utilizador deve colocar várias imagens que deseja analisar (sendo que todas devem ser do formato jpg) e o programa em poucos segundos irá criar um ficheiro txt irá conter o número de ovos presentes em cada imagem.

## Motivo

Este projeto foi motivado pela UC de Laboratório de Bioinformática.

## Requisitos
- Python
- Docker "https://hub.docker.com/repository/docker/pedro28pacheco/the-underdogs/general"

##  Autores

<p> <a href= "https://github.com/David0000001"> David Cabrita - 202100320 </a> </p>
<p> <a href= "https://github.com/SalvadorPacheco28"> Pedro Pacheco - 202100957 </a> </p>
<p> <a href= "https://github.com/TiagoBarao20"> Tiago Barão - 202000215 </a> </p>

## References

- GitHub: [Git User's Manual](https://derkling.matbug.net/_media/teaching:2010:gitusermanual.pdf)
- Python: [Python Website](https://www.python.org)
- Vscode: [VS Code Documentation](https://code.visualstudio.com/docs)
- GitLab: [GitLab Documentation](https://docs.gitlab.com)

