import cv2
import matplotlib.pyplot as plt
import numpy as np
import tkinter as tk
from tkinter import filedialog
import os

def select_region_image(imagem):
    #Tamanho da imagem original para poder adaptar o tamanho da imagem
    height, width, _ = imagem.shape
    #Adaptar a janela de acordo com o tamanho da imagem
    cv2.namedWindow("Selecione a área de interesse", cv2.WINDOW_NORMAL)
    cv2.resizeWindow("Selecione a área de interesse", width, height)
    region = cv2.selectROI("Selecione a área de interesse", imagem, fromCenter=False, showCrosshair=True)
    cv2.destroyAllWindows()
    return region


def binarize_image(cropped_image):
    # Converter a imagem para tons de cinza
    gray = cv2.cvtColor(cropped_image, cv2.COLOR_BGR2GRAY)

    # Aplicar a limiarização de Otsu
    _, binary_image = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    return binary_image

def count_eggs(binary_image):
    # Encontrar contornos na imagem binária
    contours, _ = cv2.findContours(binary_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Inicializar contador de ovos
    num_eggs = 0

    # Iterar sobre os contornos
    for contour in contours:
        # Calcular a área do contorno
        area = cv2.contourArea(contour)
        
        # Verificar se a área está entre 50 e 150 pixels^2
        if 50 <= area <= 150:
            # Incrementar o contador de ovos
            num_eggs += 1

    return num_eggs

def calculate_accuracy(original_binary_image, new_binary_image):
    # Calcular a diferença absoluta entre as duas imagens binárias
    diff = cv2.absdiff(original_binary_image, new_binary_image)
    
    # Contar o número de pixels diferentes
    num_different_pixels = np.sum(diff / 255)
    
    # Calcular a taxa de precisão
    total_pixels = original_binary_image.shape[0] * original_binary_image.shape[1]
    accuracy_rate = 1 - (num_different_pixels / total_pixels)
    
    return accuracy_rate

def verify_accuracy(original_binary_image, cropped_image):
    # Aplicar os métodos de processamento de imagem novamente
    new_binary_image = binarize_image(cropped_image)

    accuracy_rate = calculate_accuracy(original_binary_image, new_binary_image)
    
    return accuracy_rate


#Para nao aparecer a janela princial
def main():
    root = tk.Tk()
    root.withdraw()  

    # Definir o caminho para a pasta de imagens
    folder_path = (r"./Drosphilla_Egg_Counter/Imagens_ovos_Drosphilla")
    # Selecionar um arquivo jpg da pasta
    file_path = filedialog.askopenfilename(initialdir=folder_path, filetypes=[("Imagens JPG", "*.jpg")])

    if not file_path:
        print("Nenhum arquivo selecionado. O programa vai encerrar :/.")
        return

    #Carregar a imagem
    imagem = cv2.imread(file_path)

    if imagem is None:
        print("Não foi possível carregar a imagem. Verifique se o arquivo selecionado é válido.")
        return

    #Selecionar a area de interesse
    selected_region = select_region_image(imagem)

    # Cortar a area de interesse
    x, y, w, h = selected_region
    cropped_image = imagem[y:y+h, x:x+w]

    # Converter a imagem para binaria
    binary_image = binarize_image(cropped_image)

    # Contar o número de ovos na imagem binaria
    num_eggs = count_eggs(binary_image)

    # Imprimir o número de ovos
    print(f"Número de ovos na imagem: {num_eggs}")

    num_executions = 10000
    num_eggs_list = []
    accuracy_rates = []

    accuracy_choice = input("Gostaria de verificar a precisão da contagem? (sim/não): ")
    if accuracy_choice.lower() == "sim":
        for _ in range(num_executions):
            # Contar o número de ovos na imagem binária
            num_eggs = count_eggs(binary_image)
            num_eggs_list.append(num_eggs)
            # Verificar a precisão da contagem
            accuracy_rate = verify_accuracy(binary_image, cropped_image)
            accuracy_rates.append(accuracy_rate)

    # Calcular média de ovos
    avg_num_eggs = sum(num_eggs_list) / len(num_eggs_list)
    print(f"Média de ovos: {avg_num_eggs:.2f}")

    # Calcular precisão média
    if accuracy_rates:
        avg_accuracy = sum(accuracy_rates) / len(accuracy_rates)
        print(f"Taxa de precisão média: {avg_accuracy * 100:.2f}%")
    else:
        print("Nenhuma verificação de precisão foi feita.")

    return cropped_image,binary_image
if __name__ == "__main__":
    cropped_image, binary_image = main()
    if cropped_image is not None:
        cv2.imshow("Imagem recortada", cropped_image)
    if binary_image is not None:
        cv2.imshow("Imagem Binária", binary_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()



